from django import forms


from .models import Comment, UserData

class CommentForm(forms.ModelForm):

    class Meta:
        model = Comment
        fields = ('user','item','body','date')
        widgets = {'user': forms.HiddenInput(),'item': forms.HiddenInput(),
                'date': forms.HiddenInput()}

class UserDataImgForm(forms.ModelForm):
    class Meta:
        model = UserData
        fields = ('user','image')
        widgets = {'user': forms.HiddenInput()}
