from django.contrib import admin

# Register your models here.

from django.contrib import admin

# Register your models here.

from .models import Feeder,Item,UserData,Vote,Comment
from .models import YTItem, WKItem

admin.site.register(Feeder)
admin.site.register(Item)
admin.site.register(YTItem)
admin.site.register(WKItem)
admin.site.register(UserData)
admin.site.register(Vote)
admin.site.register(Comment)
