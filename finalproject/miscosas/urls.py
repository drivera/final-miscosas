from django.urls import path, include

from . import views


session_patterns = [
    path('login', views.login_view, name='login'),
    path('logout', views.logout_view, name='logout'),
    path('register', views.register_view, name="register"),
]

resources_patterns = [
    path('',views.feeder_list, name="feeder_list"),
    path('<str:feeder>', views.feeder, name="feeder"),
    path('<str:feeder>/<str:item_id>', views.item, name="item"),
    path('<str:feeder>/<str:item_id>/vote', views.vote, name="vote"),
    path('<str:feeder>/<str:item_id>/comment', views.comment, name="comment"),
]

urlpatterns = [
    path('', views.landing, name="landing"),
    path('feeders/', include(resources_patterns), name="feeder_list"),
    path('users/', views.user_list, name="user_list"),
    path('users/<str:username>', views.user, name="user"),
    path('info/', views.info, name="info"),
    path('session/', include(session_patterns)),
    path('stylesheet/', views.stylesheet, name="stylesheet"),
]
