from django.test import TestCase
from . import views
from miscosas.models import Item, Feeder, Vote, Comment, User,UserData

from django.utils import timezone
# Create your tests here.
class TestViews(TestCase):

    def setUp(self):
        self.feeder = Feeder.objects.create(feeder="UC300utwSVAYOoRLEqmsprfg", name=1, type="yt" )
        self.item = Item.objects.create(feeder=self.feeder, name="item1", link="asdasd?v=asdasd")
        self.user = User.objects.create(username="test")
        self.userdata = UserData.objects.create(style="d", user=self.user)
        self.vote = Vote.objects.create(item=self.item, user=self.user, vote="p")
        self.comment = Comment.objects.create(item=self.item, user=self.user, body="test",date=timezone.now())

    def test_landing(self):
        response = self.client.get('/miscosas/')
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.resolver_match.func, views.landing)
        self.assertInHTML('<h3 class="border-bottom pb-2 mb-0 text-center">Lo mejor valorado</h3>', response.content.decode(encoding='UTF-8'))

    def test_feeders(self):
        response = self.client.get('/miscosas/feeders/')
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.resolver_match.func, views.feeder_list)
        self.assertInHTML('<strong> Total Items: </strong>', response.content.decode(encoding='UTF-8'))

    def test_feederspost(self):
        response = self.client.post('/miscosas/feeders/',{"type":"yt", "feeder":"UCWzZ5TIGoZ6o-KtbGCyhnhg"})
        self.assertRedirects(response, '/miscosas/feeders/UCWzZ5TIGoZ6o-KtbGCyhnhg', status_code=302,
        target_status_code=200, fetch_redirect_response=True)

    def test_users(self):
        response = self.client.get('/miscosas/users/')
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.resolver_match.func, views.user_list)
        self.assertInHTML('<strong> Total Votos: </strong>', response.content.decode(encoding='UTF-8'))

    def test_info(self):
        response = self.client.get('/miscosas/info/')
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.resolver_match.func, views.info)
        self.assertInHTML('<h3 class="border-bottom pb-2 mb-0 text-center">Información de la aplicación</h3>', response.content.decode(encoding='UTF-8'))

    def test_stylesheet(self):
        response = self.client.get('/miscosas/stylesheet/')
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.resolver_match.func, views.stylesheet)

    def test_feederget(self):
        response = self.client.get('/miscosas/feeders/'+ self.feeder.feeder)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.resolver_match.func, views.feeder)
        self.assertInHTML('<h3 class="border-bottom pb-2 mb-0 text-center">Lista de Items</h3>', response.content.decode(encoding='UTF-8'))


    def test_feederpost(self):
        response = self.client.post('/miscosas/feeders/'+self.feeder.feeder ,{"action":"Select"})
        self.assertRedirects(response, '/miscosas/', status_code=302,
        target_status_code=200, fetch_redirect_response=True)

    def test_itemget(self):
        response = self.client.get('/miscosas/feeders/'+ self.feeder.feeder + "/" + str(self.item.id))
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.resolver_match.func, views.item)
        self.assertInHTML('<strong> Total Items: </strong>', response.content.decode(encoding='UTF-8'))


    def test_userget(self):
        response = self.client.get('/miscosas/users/'+ self.user.username)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.resolver_match.func, views.user)
        self.assertInHTML('<span class="line font-weight-bold">'+self.item.name +': </span>', response.content.decode(encoding='UTF-8'))


    def test_userpost(self):
        response = self.client.post('/miscosas/users/'+ self.user.username)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.resolver_match.func, views.user)

    def test_logoutpost(self):
        response = self.client.post('/miscosas/session/logout')
        self.assertRedirects(response, '/miscosas/', status_code=302,
        target_status_code=200, fetch_redirect_response=True)

    def test_loginpost(self):
        response = self.client.post('/miscosas/session/login')
        self.assertRedirects(response, '/miscosas/', status_code=302,
        target_status_code=200, fetch_redirect_response=True)

    def test_registerget(self):
        response = self.client.get('/miscosas/session/register')
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.resolver_match.func, views.register_view)
        self.assertInHTML('<button type="submit">Registrar</button>', response.content.decode(encoding='UTF-8'))

    def test_registerpost(self):
        response = self.client.post('/miscosas/session/register')
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.resolver_match.func, views.register_view)
