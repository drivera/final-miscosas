from django.shortcuts import render,loader, redirect, get_object_or_404
from django.http import HttpResponse, Http404
from django.views.decorators.csrf import csrf_exempt

from django.db.models import Count, Q, F

from django.contrib.auth.models import User
from miscosas.models import Item, Feeder, Vote, Comment

from miscosas.forms import UserDataImgForm,  CommentForm
from miscosas.compilers import YTCompiler, WKCompiler

from django.utils import timezone
from django.utils.datastructures import MultiValueDictKeyError

@csrf_exempt
def landing(request):
    formatpages={
        'xml': "landing.xml",
        'json':"landing.json",
    }
    formats={
        'xml': "text/xml",
        'json':"application/json",
    }

    i = Item.objects.annotate(positive=Count('vote',filter=Q(vote__vote='p'),
        distinct=True),negative=Count('vote',filter=Q(vote__vote='n'),
        distinct=True),score=F('positive')-F('negative')).order_by('-score')[:10]
    f = Feeder.objects.filter(selected=True)

    context = {
        'items': i,
        'feeders': f,
    }

    if request.user.is_authenticated:
            lastv = Vote.objects.filter(user=request.user).order_by('-modified_date')[:5]
            itemsids = i.values_list('id', flat=True)

            v = Item.objects.filter(id__in=itemsids).filter(Q(vote__user = request.user) |
                Q(vote__isnull=True)).values_list('id', 'vote__vote')

            context['lastv']=lastv
            context['votes'] = dict((x, y) for x, y in v)

    format = request.GET.get("format")
    template = loader.get_template('miscosas/'+ formatpages.get(format,"landing.html"))
    html = template.render(request=request, context=context)
    return HttpResponse(html,content_type=formats.get(format))


def feeder_list(request):
    compilers={
        'yt': YTCompiler,
        'wk': WKCompiler,
    }

    if request.method=="POST":
        try:
            type = request.POST['type']
            feeder = request.POST['feeder']
        except:
            raise Http404("qs from Post contains incorrect entry")
        try:
            compilers[type](feeder)
        except IOError:
            raise Http404("Feeder not found")

        return redirect("/miscosas/feeders/" + feeder )

    elif request.method =="GET":

        f = Feeder.objects.all()

        context = {'feeders': f}

        return render(request, 'miscosas/feeder_list.html',context)

def user_list(request):
    users = User.objects.all()
    users = users.annotate(tvotes=Count('vote', distinct=True),
                        tcomments=Count('comment', distinct=True))

    context={"users":users}

    return render(request, 'miscosas/user_list.html',context)

def info(request):
    return render(request, "miscosas/info.html")

def stylesheet(request):
    template = loader.get_template("miscosas/stylesheet.css")
    html = template.render(request=request)
    response = HttpResponse(html,content_type='text/css')
    return response

def feeder(request, feeder):

    f = get_object_or_404(Feeder,feeder=feeder)

    if request.method=="POST":
            if request.POST['action'] == "Select":
                f.selected = True;
            elif request.POST['action'] == "Deselect":
                f.selected = False
            f.save()

            try:
                return redirect(request.POST['path'])
            except MultiValueDictKeyError:
                return redirect('/miscosas/')

    i = Item.objects.filter(feeder=f).order_by('-id')

    context = {
        'feeder': f,
        'items': i,
    }
    return render(request,"miscosas/feeder.html",context)

def item(request, feeder, item_id):

    itemtypes={
        'yt': 'ytitem.html',
        'wk': 'wkitem.html',
    }

    f = get_object_or_404(Feeder,feeder=feeder)
    i = get_object_or_404(Item, feeder=f, id=item_id)

    c = Comment.objects.filter(item=i)

    context = {
        'feeder':f,
        'item':i,
        'comments':c,
    }

    if request.user.is_authenticated:
        try:
            v = Vote.objects.get(user=request.user,item=i)
        except Vote.DoesNotExist:
            pass
        else:
            context['vote']=v

        context['form'] = CommentForm(initial={'item': i,
                                        'user': request.user,
                                        'date': timezone.now()})

    return render(request, 'miscosas/'+ itemtypes[f.type],context)

def user(request, username):
    user = get_object_or_404(User,username=username)

    if request.method=="POST" and request.user.is_authenticated:
        if 'style' in request.POST:
            request.user.userdata.style = request.POST["style"]
            request.user.userdata.letter = request.POST["letter"]
            request.user.userdata.save()
        else:
            form = UserDataImgForm(request.POST, request.FILES,
                                    instance=user.userdata)
            form.save()

    votes = Vote.objects.filter(user=user)
    comments = Comment.objects.filter(user=user)
    context={
        'userdata':user,
        'votes':votes,
        'comments':comments,
    }

    if request.user.is_authenticated:
        context['form'] = UserDataImgForm(instance=user.userdata)

    return render(request, 'miscosas/user.html',context)
