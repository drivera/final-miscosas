from django.shortcuts import render, redirect

from django.contrib.auth import login, logout, authenticate
from django.contrib.auth.forms import UserCreationForm

from django.utils.datastructures import MultiValueDictKeyError
from django.contrib import messages

from miscosas.models import UserData

def logout_view(request):
    if request.method == "POST":
        logout(request)

        try:
            return redirect(request.POST['path'])
        except MultiValueDictKeyError:
            return redirect('/miscosas/')

def login_view(request):
    if request.method == "POST":
        username = request.POST.get('username')
        password = request.POST.get('password')

        user = authenticate(request, username=username,
                            password=password)

        if user is not None:
            login(request,user)
        else:
            messages.error(request, "El usuario no existe")

        try:
            return redirect(request.POST['path'])
        except MultiValueDictKeyError:
            return redirect('/miscosas/')

def register_view(request):
    form = UserCreationForm()
    form.fields['username'].help_text = None
    form.fields['password1'].help_text = None
    form.fields['password2'].help_text = None

    if request.method == "POST":
        form = UserCreationForm(data=request.POST)
        if form.is_valid():
            user = form.save()
            ud = UserData(user=user)
            ud.save()

            if user is not None:
                login(request, user)
                return redirect('/miscosas/')

    context = {'form': form}
    return render(request, "miscosas/register.html", context)
