from django.shortcuts import render, redirect, get_object_or_404
from django.http import Http404

from django.utils import timezone

from miscosas.models import Item, Vote, Feeder
from miscosas.forms import CommentForm

def comment(request,feeder, item_id):
    f = get_object_or_404(Feeder, feeder=feeder)
    get_object_or_404(Item, id=item_id, feeder=f)

    if request.method == "POST":
        form = CommentForm(request.POST)
        if form.is_valid():
            instance = form.save(commit=False)
            instance.date = timezone.now()
            instance.save()

            return redirect('/miscosas/feeders/' + feeder + '/' + item_id)
        return Http404("Comment couldn't be created")

def vote(request,feeder, item_id):

    f = get_object_or_404(Feeder, feeder=feeder)
    i = get_object_or_404(Item, id=item_id, feeder=f)

    if request.method == "POST":
        if request.user.is_authenticated:

            try:
                v = Vote.objects.get(user=request.user,item=i)
            except Vote.DoesNotExist:
                v = Vote(user=request.user, item=i)

            if request.POST['action'] == "positive":
                v.vote = 'p';
            elif request.POST['action'] == "negative":
                v.vote = 'n'

            v.save()

        try:
            return redirect(request.POST['path'])
        except MultiValueDictKeyError:
            return redirect('/miscosas')
