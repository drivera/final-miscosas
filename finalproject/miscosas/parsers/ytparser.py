from xml.sax.handler import ContentHandler
from xml.sax import make_parser
import sys
import string

class YTHandler(ContentHandler):

    def __init__ (self):
        self.inEntry = False
        self.inContent = False
        self.theContent = ""

        self.channel = {}
        self.videos = []

        self.clink = ""
        self.cname =""

        self.title = ""
        self.url = ""
        self.description = ""

    def startElement (self, name, attrs):
        if name == 'entry':
            self.inEntry=True

        elif self.inEntry:
            if name == "title":
                self.inContent = True
            elif name == 'link':
                self.url = attrs.get('href')
            elif name =="media:description":
                self.inContent = True

        elif not self.inEntry:
            if name == "link":
                self.clink = attrs.get('href')
            if name == "title":
                self.inContent = True


    def endElement (self, name):
        if name == 'entry':
            self.inEntry=False
            self.videos.append({'title': self.title,
                                'link': self.url,
                                'description':self.description})

        elif name == 'feed':
            self.channel = {'cname': self.cname,
                            'clink': self.clink}

        elif self.inEntry:
            if name == 'title':
                self.title = self.theContent
            elif name =="media:description":
                self.description = self.theContent

            self.inContent = False
            self.theContent = ""

        elif not self.inEntry:
            if name == 'title':
                self.cname = self.theContent
                self.inContent = False
                self.theContent = ""

    def characters (self, chars):
        if self.inContent:
            self.theContent = self.theContent + chars


class YTParser():
    def __init__(self, stream):
        self.parser = make_parser()
        self.handler = YTHandler()
        self.parser.setContentHandler(self.handler)
        self.parser.parse(stream)

    def videos (self):
        return self.handler.videos
    def channel (self):
        return self.handler.channel
