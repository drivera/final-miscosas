from xml.sax.handler import ContentHandler
from xml.sax import make_parser
import sys
import string

class WKHandler(ContentHandler):

    def __init__ (self):
        self.inEntry = False
        self.inContent = False
        self.theContent = ""

        self.wiki = {}
        self.items = []

        self.wlink = ""
        self.wname =""

        self.title = ""
        self.url = ""
        self.date= ""
        self.publisher = ""

    def startElement (self, name, attrs):
        if name == 'item':
            self.inEntry=True

        elif self.inEntry:
            if name == "title":
                self.inContent = True
            elif name == 'link':
                self.inContent = True
            elif name =="pubDate":
                self.inContent = True
            elif name =="dc:creator":
                self.inContent = True

        elif not self.inEntry:
            if name == "link":
                self.inContent = True
            if name == "title":
                self.inContent = True


    def endElement (self, name):
        if name == 'item':
            self.inEntry=False
            self.items.append({'title': self.title,
                                'link': self.url,
                                'date':self.date,
                                'publisher': self.publisher})

        elif name == 'rss':
            self.wiki = {'wname': self.wname,
                            'wlink': self.wlink.split("&action=history")[0]}

        elif self.inEntry:
            if name == "title":
                self.title  = self.theContent
            elif name == 'link':
                self.url = self.theContent
            elif name =="pubDate":
                self.date = self.theContent
            elif name =="dc:creator":
                self.publisher = self.theContent

            self.inContent = False
            self.theContent = ""

        elif not self.inEntry:
            if name == 'title':
                self.wname = self.theContent
            if name == "link":
                self.wlink  = self.theContent

            self.inContent = False
            self.theContent = ""

    def characters (self, chars):
        if self.inContent:
            self.theContent = self.theContent + chars


class WKParser():
    def __init__(self, stream):
        self.parser = make_parser()
        self.handler = WKHandler()
        self.parser.setContentHandler(self.handler)
        self.parser.parse(stream)

    def items (self):
        return self.handler.items
    def wiki (self):
        return self.handler.wiki
