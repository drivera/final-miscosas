import urllib.request

from miscosas.models import Feeder,YTItem
from miscosas.parsers.ytparser import YTParser

link_structure = "https://www.youtube.com/feeds/videos.xml?channel_id="

def YTCompiler(feeder):
    feed_link = link_structure + feeder

    try:
        xmlStream = urllib.request.urlopen(feed_link)
    except urllib.error.HTTPError:
        raise IOError("URL can't open")

    yt = YTParser(xmlStream)
    channel = yt.channel()
    videos = yt.videos()

    try:
        f = Feeder.objects.get(link=channel['clink'])
    except Feeder.DoesNotExist:
            f = Feeder(feeder=feeder, link=channel['clink'],
                        name=channel['cname'], type='yt')
            f.save()

    videos = reversed(videos)
    for v in videos:
        try:
            i = YTItem.objects.get(feeder=f, link=v['link'])
        except YTItem.DoesNotExist:
            i = YTItem(feeder=f, name=v['title'],
                        link=v['link'],
                        description=v['description'])
            i.save()
