import urllib.request

from miscosas.models import Feeder,WKItem
from miscosas.parsers.wkparser import WKParser

link_structure = "https://en.wikipedia.org/w/index.php?title={feeder}&action=history&feed=rss"

def WKCompiler(feeder):
    feed_link = link_structure.format(feeder=feeder)

    try:
        xmlStream = urllib.request.urlopen(feed_link)
    except urllib.error.HTTPError:
        raise IOError("URL can't open")

    wk = WKParser(xmlStream)
    wiki = wk.wiki()
    items = wk.items()

    try:
        f = Feeder.objects.get(link=wiki['wlink'])
    except Feeder.DoesNotExist:
            f = Feeder(feeder=feeder, link=wiki['wlink'],
                        name=wiki['wname'], type='wk')
            f.save()

    items = reversed(items)
    for i in items:
        try:
            i = WKItem.objects.get(feeder=f,link=i['link'])
        except WKItem.DoesNotExist:
            i = WKItem(feeder=f, name=i['title'],
                        link=i['link'],
                        author=i['publisher'],
                        date=i['date'])
            i.save()
