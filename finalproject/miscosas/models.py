from django.db import models
from django.contrib.auth.models import User

from django.db.models import Sum,Count, Case, Q, F

class Feeder(models.Model):
    TYPES = [
        ('yt', 'Youtube'),
        ('wk', 'Wikipedia'),
    ]

    feeder = models.CharField(max_length=128)
    name = models.CharField(max_length=128)
    link = models.CharField(max_length=256)
    type = models.CharField(max_length=2, choices=TYPES)
    selected = models.BooleanField(default=True)

    def __str__(self):
        return "(" + self.type + ") " + self.name
    def nitems(self):
        return Feeder.objects.annotate(ni=Count('item',distinct=True)).get(id=self.id).ni
    def tscore(self):
        return Feeder.objects.annotate(p=Count('item__vote',filter=Q(item__vote__vote='p'),distinct=True),n=Count('item__vote',filter=Q(item__vote__vote='n'),distinct=True),ts=F('p')-F('n')).get(id=self.id).ts

class Item(models.Model):
    feeder = models.ForeignKey(Feeder, on_delete=models.CASCADE)
    name = models.CharField(max_length=128)
    link = models.CharField(max_length=256)

    def __str__(self):
        return str(self.feeder) + ": " + self.name

    def pscore(self):
        return Item.objects.annotate(p=Count('vote',filter=Q(vote__vote='p'))).get(id=self.id).p

    def nscore(self):
        return Item.objects.annotate(n=Count('vote',filter=Q(vote__vote='n'))).get(id=self.id).n

class YTItem(Item):
    description = models.TextField()

class WKItem(Item):
    author = models.CharField(max_length=128)
    date = models.CharField(max_length=128)

class UserData(models.Model):
    STYLE = [
        ('l', 'ligth'),
        ('d', 'dark'),
    ]
    LETTER = [
        ('s', 'small'),
        ('m', 'medium'),
        ('b', 'big')
    ]

    user = models.OneToOneField(User, on_delete=models.CASCADE)
    style = models.CharField(max_length=1, choices=STYLE, default='l')
    letter = models.CharField(max_length=1, choices=LETTER, default='m')
    image = models.ImageField(upload_to = "user_imgs", default="user_imgs/usuario.png")

    def __str__(self):
        return str(self.user)

class Vote(models.Model):
    VOTE = [
        ('p', 'positive'),
        ('n', 'negative')
    ]

    user = models.ForeignKey(User, on_delete=models.CASCADE)
    item  = models.ForeignKey(Item, on_delete=models.CASCADE)
    vote = models.CharField(max_length=1, choices=VOTE)
    modified_date = models.DateTimeField(auto_now=True)

    def __str__(self):
        return str(self.item) + " - " + str(self.user) + " - " + self.vote

class Comment(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    item  = models.ForeignKey(Item, on_delete=models.CASCADE)
    body = models.TextField(blank=False)
    date = models.DateTimeField('publicado')

    def __str__(self):
        return str(self.user) + " - " + str(self.item) + ": " + str(self.date)
