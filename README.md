# Entrega practica

## Datos

* Nombre: Daniel Rivera Paniagua
* Titulación: Grado en Ingeniería en Tecnologías de la Telecomunicación
* Nombre de Cuenta: drivera
* Video básico (url): https://www.youtube.com/watch?v=WD1Uy7iADE4&feature=youtu.be
* Video parte opcional (url): https://www.youtube.com/watch?v=ruefw2-Mb_c&feature=youtu.be
* Despliegue (url): http://danirivera.pythonanywhere.com/miscosas/
## Cuenta Admin Site

* admin/admin-User

## Cuentas usuarios

* juanj0/juan-User
* lauu/lau-User
* mark/mark-User

## Resumen parte obligatoria

* Todos los items cuentan siempre con un enlace para ir directos a sus páginas.
* Todos los alimentadores clikando en el nombre te llevan a su página
* Se han usado unos marcadores para las votaciones que cambian de color en base al voto del usuario si está autenticado, si no no se pueden pulsar.
* La página de registro tiene una verificación, si hay problemas con el registro, saca las ayudas para el mismo.
* Las vistas están dividas en varios archivos que relacionan funciones finales similares.
* Cada alimentador tiene su propio parser y compilador para ordenarlo.
* La página de cada item se encuentra separada en sus partes generales y específicas como ayuda al orden y búsqueda de la abstracción
* Para obtener algunas de las informaciones he recurrido a la creación de nuevas templates tags.
* Hago uso de formularios para los comentarios y la fotografía de los usuarios.
* Cada formato de la página principal tiene su propia template.

## Lista partes opcionales

* Añadido el favicon.ico para toda la aplicación, incluso la interfaz de admin (trabajado como un static al que se redirige desde el urls.py general)
* Trabajo con bootstrap 4
  * El trabajo del css lo complemeto con bootstrap para ayduar a dar formato a toda la aplicación.
  * Uso navbar para la parte superior, y nav para el navegador por la aplicación.
  * Siempre las listas forman parte del componente media lo que ayuda a darles un formato concreto.
  * Los formularios son también hechos con bootstrap para poderles dar una forma mejorada.
  * En todas las partes se pueden encontrar clases de bootstrap con las que he solventado sobrecargar mi archivo css de estilo.
